from django.shortcuts import render

# Create your views here.
from lab_2.models import Note
from lab_4.forms import NoteForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):
    notes = Note.objects.all().values()  
    response = {'notes': notes}
    return render(request, 'lab5_index.html', response)

