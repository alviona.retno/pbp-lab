from django.urls import path
from .views import add_friend, index


urlpatterns = [
    path('', index, name='index'), # admin 
    # address - method views itu apa yang dipanggil
    path('add', add_friend, name='add')
]
