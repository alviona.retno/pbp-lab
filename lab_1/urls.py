from django.urls import path
from .views import index, friend_list

urlpatterns = [
    path('', index, name='index'), # admin 
    # TODO Add friends path using friend_list Views
    # address - method views itu apa yang dipanggil
    path('friends/', friend_list, name='friend_list')
]
