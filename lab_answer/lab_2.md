1. Apakah perbedaan antara JSON dan XML?
JSON merupakan format data turunan dari bahasa pemrograman Javascript, sehingga sintaksnya merupakan turunan dari Object JavaScript. Json umumnya digunakan untuk representasi objek. Sedangkan XML merupakan bahasa markup yang menggunakan struktur untuk representasi item data. Penulisan XML juga menggunakan tag awal dan akhir (mirip seperti tag pada HTML).
JSON lebih mudah dibuat (ringkas, karena tidak membutuhkan tag pembuka dan penutup) serta lebih mudah dipahami. 

2. Apakah perbedaan antara HTML dan XML?
XML digunakan untuk membawa file data dan tidak memungkinkan menampilkan data. Memiliki bahasa markup yang mudah dimengerti oleh manusia. XML juga merupakan bahasa markup yang ekstensif, sama seperti HTML dan format filenya mudah dibaca dan ditulis. Tag XML memiliki nama tag yang tidak ditentukan sebelumnya, sedangkan HTML memiliki nama tag yang sudah ditentukan sebelumnya, seperti head, body, header, h1, p, dll. Perbedaan utama antara HTML dan XML adalah bahwa HTML adalah Hypertext Markup Language yang membantu mengembangkan struktur halaman web sementara XML adalah Extensible Markup Language yang membantu untuk bertukar data antar platform yang berbeda.

sumber referensi :
- budisma.net
- niagahoster