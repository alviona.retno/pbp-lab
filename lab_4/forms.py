from django import forms

# Create your models here.
from lab_2.models import Note


class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = '__all__'
        labels = {'to': "To", "fromm": "From", "title": "Title", "message":"Message"}